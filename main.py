# Flight Finder Version 1.1.0
# Copyright (C) 2019  o355
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

from selenium import webdriver
import itertools
import time
import math
import sys

cached_legs = {}

best_option = []

print("- - - - - - - - - - - - - - - - - - -")
print("Welcome to Flight Finder (v1.1.0).")
print("Please enter the airport code you'd like to start at.")
start_airport = input("Enter here: ")
print("- - - - - - - - - - - - - - - - - - -")
print("Please enter a list of airport codes you'd like to visit, separated by a comma and space (e.g. JFK, EWR)")
destinations = input("Enter here: ").split(", ")
destinations_len = len(destinations)

if destinations_len >= 9:
    print("- - - - - - - - - - - - - - - - - - -")
    print("You've chosen %s airports, which would permutate into %s combinations." % (destinations_len, math.factorial(destinations_len)))
    print("This may take a long time. The script will stop for 5 seconds, please enter Ctrl+C if you would not like to continue.")
    time.sleep(5)

print("- - - - - - - - - - - - - - - - - - -")
print("Please enter the date of the flights you want in the YYYY-MM-DD format (e.g. 2019-11-01)")
date = input("Enter here: ")
print("- - - - - - - - - - - - - - - - - - -")
print("Please enter the Google Flights option string you'd like to use.")
print("You can get this option string by customizing Google Flights filtering to your choosing, then copying everything after the semicolon in the address bar.")
print("You can also choose from a pre-defined set of options below. (one-way trips only please!)")
print("----")
print("0: Economy, No carry-on bags, nonstop")
print("1: Economy, 1 carry-on bag, nonstop")
print("2: Economy, 1 carry-on bag, up to 1 stop")
print("3: Premium economy, nonstop")
print("4: Premium economy, up to 1 stop")
gflights_options = input("Input here: ")
if gflights_options == "0":
    print("Please note: Some fares generated may give you a carry-on bag even though you defined none.")
    gflights_options = "c:USD;e:1;s:0;sd:1;t:f;tt:o"
elif gflights_options == "1":
    gflights_options = "b:1;c:USD;e:1;s:0;sd:1;t:f;tt:o"
elif gflights_options == "2":
    gflights_options = "b:1;c:USD;e:1;s:1;sd:1;t:f;tt:o"
elif gflights_options == "3":
    gflights_options = "c:USD;e:1;s:0;sc:p;sd:1;t:f;tt:o"
elif gflights_options == "4":
    gflights_options = "c:USD;e:1;sc:p;sd:1;t:f;tt:o"
print("- - - - - - - - - - - - - - - - - - -")
print("Starting flightfinder.")
print("Starting web driver.")
# Option for having web driver be headless will come soon.
options = webdriver.FirefoxOptions()
driver = webdriver.Firefox(executable_path='put/your/path/here', options=options)
print("Airports listed: %s" % destinations)
print("- - - - - - - - - - - - - - - - - - -")
counter = 0
maxcounter = math.factorial(destinations_len)
for item in itertools.permutations(destinations):
    price = 0
    counter = counter + 1
    item = list(item)
    item.insert(0, start_airport)
    item.append(start_airport)
    pretty_leg = start_airport
    for i in range(1, len(item)):
        pretty_leg = pretty_leg + "-" + item[i]

    print("Testing flight combination %s/%s -> %s\r" % (str(counter), maxcounter, pretty_leg)),
    for i in range(0, len(item) - 1):
        start_arpt = item[i]
        end_arpt = item[i+1]
        leg_str = start_arpt + "-" + end_arpt
        if leg_str in cached_legs:
            price = price + cached_legs[leg_str]
            continue
        else:
            #print("Leg not in cache, fetching data...")
            driver.get("https://www.google.com/flights#flt=%s.%s.%s;%s" % (start_arpt, end_arpt, date, gflights_options))
            time.sleep(1.5)
            matches = driver.find_elements_by_class_name('gws-flights-results__cheapest-price')
            try:
                legprice = matches[0].text
                legprice = int(legprice.replace("$", "").replace(",", ""))
                price = price + legprice
                cached_legs['%s' % leg_str] = legprice
                continue
            except:
                print("!\r")
                legprice = 99999
                cached_legs['%s' % leg_str] = legprice
                price = price + legprice
                continue


    if len(best_option) == 0:
        best_option.append({pretty_leg: price})
    else:
        for key in best_option[0]:
            best_price = best_option[0][key]

        if best_price == price:
            best_option.append({pretty_leg: price})
        elif best_price > price:
            best_option = []
            best_option.append({pretty_leg: price})
print("- - - - - - - - - - - - - - - - - - -")
print("Processing done.")
print("These are the best options:")
for i in range(0, len(best_option)):
    for key in best_option[i]:
        print("%s at a price of $%s" % (key, str(best_option[i][key])))
