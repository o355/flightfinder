# Flight Finder
Finds flights fast. Licensed under the GNU GPL v3.

# What is this?
An experimental project using Selenium & Google Flights to find the best flight routing for a set route of airports (graph theory).

# Is it actually useful?
Sort of, but there are lots of...

# Limitations
* Currently flightfinder uses a brute-force algorithm, and given that this is coded in Python it is slow all things considered. Python being single-core doesn't help either.
* Doing an 11 airport combination is about as far as you can go before run times start going into the days or weeks.
* This script is Cythonable but I'm not sure if it really makes a difference
* You'll need to modify the code where the webdriver is specified to point to a geckodriver executable (https://github.com/mozilla/geckodriver/releases). You also need Firefox installed. (planning to add a config file so that you don't have to hard-code this into the code.)
* This script just finds prices for one specific day. It doesn't factor in travel time, or time to stop in each city (although this is planned for a future release)

# Upcoming features (when I can find time)
* Having a flight finder optimized for nearest neighbor (prices may be a bit higher than actual but the time it takes to do all the computations will be decreased)
* Supporting date changes and trying to figure it out in a smart manner
* Running selenium in headless mode
* Cleaning up the code

# How to use
You'll need Python 3 and selenium and itertools (selenium needs a pip install, itertools possibly?).

To start enter the code of the airport you want to start at. You can get this code from google flights.

Then enter the codes of the airports you want to travel to, separated by a comma and a space. Again, find these codes from google flights.

Then, enter the date that flightfinder will find the flights for. This needs to be in YYYY-MM-DD format (for January 1, 2020, it would be 2020-01-01).

After that, select the google flights filters. 5 filters are provided, but you can configure google flights filters yourself, then copy the options string as instructed by the code.

After that, flight finder will then run. Make sure you keep monitoring it in the first few minutes, as you may encounter errors where flights aren't available with the filter settings that you chose.

If this does happen, try using weaker filters (economy class, no carry on bags, 1 stop, etc).

